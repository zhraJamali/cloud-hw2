const axios = require("axios");
const fs = require("fs");
const path = require("path");
const crypto = require("crypto");

const serverUrl = "http://server:3000";

async function insertPerson(person) {
  const response = await axios.post(`${serverUrl}/persons`, person);
  return response.data;
}

async function getPersons() {
  const response = await axios.get(`${serverUrl}/persons`);
  return response.data;
}
``;
async function getPersonById(id) {
  const response = await axios.get(`${serverUrl}/persons/${id}`);
  return response.data;
}

async function removePerson(id) {
  const response = await axios.delete(`${serverUrl}/persons/${id}`);
  return response.data;
}

async function getFile() {
  const response = await axios.get(`${serverUrl}/file`, {
    responseType: "arraybuffer",
  });
  const fileBuffer = Buffer.from(response.data);
  const checksum = crypto.createHash("md5").update(fileBuffer).digest("hex");
  const expectedChecksum = "INSERT_EXPECTED_CHECKSUM_HERE";
  if (checksum === expectedChecksum) {
    fs.writeFileSync(path.join("/clientdata", "filename"), fileBuffer);
    return true;
  } else {
    return false;
  }
}

async function main() {
  const person = {
    name: "John Doe",
    age: 30,
  };
  const insertedPerson = await insertPerson(person);
  console.log("Inserted person:", insertedPerson);

  const persons = await getPersons();
  console.log("All persons:", persons);

  const personById = await getPersonById(insertedPerson._id);
  console.log("Person by ID:", personById);

  const removedPerson = await removePerson(insertedPerson._id);
  console.log("Removed person:", removedPerson);

  const fileReceived = await getFile();
  console.log("File received:", fileReceived);
}

main();
