const express = require("express");
const path = require("path");
const logger = require("morgan");
const bodyParser = require("body-parser");
const crypto = require("crypto");
const MongoClient = require("mongodb").MongoClient;
const randomstring = require("randomstring");
const multer = require("multer");
const fs = require("fs");

const app = express();

// Set up MongoDB connection
MongoClient.connect("mongodb://localhost:27017/", function (err, client) {
  if (err) throw err;
  const db = client.db("mydb");
  const personCollection = db.collection("person");

  // Create schema for Person
  personCollection.createIndexes([{ key: { id: 1 }, unique: true }]);

  // Insert a new person into the collection
  app.post("/insert_person", function (req, res) {
    const person = req.body;
    person.id = randomstring.generate(16);
    personCollection.insertOne(person, function (err, result) {
      if (err) throw err;
      res.status(201).send();
    });
  });

  // Get a list of all persons in the collection
  app.get("/get_persons", function (req, res) {
    personCollection.find().toArray(function (err, docs) {
      if (err) throw err;
      res.json(docs);
    });
  });

  // Get a person by ID from the collection
  app.get("/get_person/:id", function (req, res) {
    const query = { id: req.params.id };
    personCollection.findOne(query, function (err, doc) {
      if (err) throw err;
      if (doc) {
        res.json(doc);
      } else {
        res.status(404).send();
      }
    });
  });

  // Remove a person by ID from the collection
  app.delete("/remove_person/:id", function (req, res) {
    const query = { id: req.params.id };
    personCollection.deleteOne(query, function (err, result) {
      if (err) throw err;
      if (result.deletedCount === 1) {
        res.status(200).send();
      } else {
        res.status(404).send();
      }
    });
  });

  // Generate a file with random text data and return it to the client
  app.get("/get_file", function (req, res) {
    const data = randomstring.generate(1024);
    const checksum = crypto.createHash("sha256").update(data).digest("hex");
    fs.writeFile("/serverdata/serverfile.txt", data, function (err) {
      if (err) throw err;
      res.json({ data: data, checksum: checksum });
    });
  });
});

// Set up middleware for parsing request bodies
app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Set up middleware for serving static files
app.use(express.static(path.join(__dirname, "public")));

// Catch 404 and forward to error handler
app.use(function (req, res, next) {
  const err = new Error("Not Found");
  err.status = 404;
  next(err);
});

// Error handler
app.use(function (err, req, res, next) {
  // Set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // Render the error page
  res.status(err.status || 500);
  res.render("error");
});

// Start server
const port = process.env.PORT || 3000;
app.listen(port, function () {
  console.log(`Server listening on port ${port}`);
});
