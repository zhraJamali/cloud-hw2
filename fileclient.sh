#!/bin/bash
docker build -t my-client-image client/.

docker network create my-network

docker build -t client-image client/.

docker volume create clientvol

docker run -it --name client-container --network my-network -v clientvol:/clientdata my-client-image
