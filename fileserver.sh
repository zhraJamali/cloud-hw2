#!/bin/bash

docker build -t my-server-image server/.

docker volume create servervol

docker run --name server --network my-network -p 3000:3000 -v servervol:/serverdata my-server-image
