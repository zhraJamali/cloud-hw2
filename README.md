# Homework OF Server Client Docker Image

This is an demonstration about how to run both images for server and client and connect them
to same network with a volume for each image


## Prerequisites

- Docker installed on your machine
- Basic knowledge of Docker commands

## How to Use

1. Clone this repository on your local machine.

2. Build the server image by running the following command in your terminal:

```bash
docker build -t my-server-image server/.
```

3. Create a Docker volume for the server by running the following command:

```bash
docker build -t my-server-image server/.
```

4. Run the server container with the following command:

```bash
docker run --name server --network my-network -p 3000:3000 -v servervol:/serverdata my-server-image
```

5. Open another terminal window and navigate to the cloned repository.

6. Build the client image by running the following command in your terminal:

```bash
docker build -t my-client-image client/.
```

7. Create a Docker network by running the following command in your terminal:

```bash
docker network create my-network
```

8. Create a Docker volume for the client container:

```bash
docker volume create clientvol
```

9. Run the client container with the following command:

```bash
docker run -it --name client-container --network my-network -v clientvol:/clientdata my-client-image
```
